class Comment < ActiveRecord::Base
  belongs_ to :post
  validates_presence_of :post_id
  validates_presence_of :body
  attr_accessible :body, :post_id
end
